from socket import socket, AF_INET, SOCK_DGRAM
import select, sys, time
from threading import Thread
import struct
import optparse

workers = []
vivos = []
detectados = []
ip = ''
sock = socket(AF_INET,SOCK_DGRAM)
sockOut = socket(AF_INET,SOCK_DGRAM)

class MyThread(Thread):
	def __init__(self):
		Thread.__init__(self)
	def run(self):
		timeout()
		
class Keyboard(): #objecte que correspondra al teclat
    def __init__(self):
        self.reader = sys.stdin
    def fileno(self):
        return self.reader.fileno()
    def on_read(self):
        txt = self.reader.readline().split('\n')[0]
        txt = txt.split()
        if len(txt) > 0:
            if txt[0] == "stat":
                print ("\n********************* Workers Status *********************")
                print( "Lider: ", lider)
                print ("  Worker        status")
                print ("  -------       -----")
                for worker in workers:
                        if worker in detectados:
                        	print(worker, "    ", "DEAD")
                        else:
                        	print(worker, "    ", "ALIVE")
                print ("\n**********************************************************")
            if txt[0] == "v":
            	print(vivos)
            if txt[0] == "d":
            	print(detectados)




def comencar():
	global sockOut, sock
	sockOut.bind((ip,6000))
	debug('opening socket, ip: ' + str(ip) + ' port ' + str(6001)) 
	print ("your IP: ", ip)
	sock.bind((ip,6001))

	global vivos, detectados
	vivos = workers
	detectados = []

def get_ip():

    s = socket(AF_INET, SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    finally:
        s.close()
    return IP

def timeout():
	global vivos, detectados
	while True:
		for candidate in workers:
			if candidate not in vivos and candidate not in detectados:
				detectados.append(candidate)
				
			askAlive(candidate)
		vivos = []
		#clear()
		time.sleep(2)

	

def serviceLoop():
	global vivos
	keyboard = Keyboard()
	while True:
		readers ,_,_ = select.select([sock,keyboard],[],[])
		for reader in readers:
			if reader == sock:
				txt, (addr, _) = sock.recvfrom(1024)
				if txt.decode('utf-8') == 'HeartbeatReply' :
					if addr not in vivos: 	
						vivos.append(addr)
					if addr in detectados:
						detectados.pop(detectados.index(addr))
				elif txt.decode('utf-8') == 'HeartbeatRequest':
					if addr not in workers:
						workers.append(addr)
					askImAlive(addr)
				else:
					sockOut.sendto(txt,('localhost',4567))
			else:
				keyboard.on_read()



def askAlive(worker):
	code = "HeartbeatRequest".encode('utf-8')
	debug('Sending: ' + str(type(code[0])) + ' to ' + worker + ' port ' + str(6001))
	sockOut.sendto(code,(worker,6001))

def askImAlive(worker):
	code = "HeartbeatReply".encode('utf-8')
	debug('Sending: ' + str(code[0]) + ' to ' + worker + ' port ' + str(6001))
	sockOut.sendto(code,(worker,6001))

def falha(worker):
	print (worker,"dead")

def debug(msg):
	if not 1:
		print(msg)


def init(_workers):
	global workers, ip
	for key in _workers:
		workers += [ _workers[key] ]
	ip = get_ip()
	comencar()
	myT = MyThread()
	myT.start()
	serviceLoop()
