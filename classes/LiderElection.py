import classes.FailureDetection as fd

from threading import Thread
import time


codes=['HeartbeatRequest','HeartbeatReply','ProcessRequest','ProcessAnswerNo','ProcessAnswerYes;num','InterruptProces','Process;Inicio;Fin']
workers = {2: '192.168.0.33',1:'192.168.0.23'}
lider = ''
imLider = False

class FalhasThread(Thread):
	def __init__(self):
		Thread.__init__(self)
	def run(self):
		fd.init(workers)


def setLider():
	global lider,imLider
	b=[]
	for key in workers:
		if workers[key] not in fd.detectados:
			b += [key]
	if b:
		b = min(b)

		if lider != workers[b]:
			lider = workers[b]
			if lider == fd.ip:
				imLider = True
				print("you are the new lider:", lider, imLider)
			else:
				imLider = False
				print("new lider: ", lider)
			fd.lider = lider
		if lider == fd.ip and not imLider:
			imLider = True
	 
def initLider():
	global lider, imLider
	b=[]
	for key in workers:
		b += [key]
	b = min(b)
	print("lowest b:",b)
	if b:
		lider = workers[b]		
		fd.lider=lider
		if lider == fd.ip:
			imLider = True
			print("you are the new lider:", lider, imLider)
	print("init lider: ",lider, imLider)


def init():	
	myT = FalhasThread()
	myT.start()
	initLider();
	while True:
		time.sleep(2)
		setLider();