import classes.LiderElection as ld
from threading import Thread
import time,datetime, hashlib,requests
from socket import AF_INET,SOCK_DGRAM,socket
import sys

actualWork = ''
url = 'https://mineracao-facens.000webhostapp.com/request.php'
now = datetime.datetime.now()
sock = socket(AF_INET,SOCK_DGRAM)
sockOut = socket(AF_INET,SOCK_DGRAM)
sock.bind(('localhost',4567))
rang = sys.maxsize
timestamp = ''
startRange = rang // len(ld.workers)
tries = 0

class LiderThread(Thread):
	def __init__(self):
		Thread.__init__(self)
	def run(self):
		ld.init()

def serviceLoop():
	while(1):
		if ld.imLider:
			liderWork()
		else:
			worker()


def worker():
	while not ld.imLider:
		work = askToLider()
		if not ld.imLider:
			print (work)
			phash = work['hash']
			nonce = int(work['start'])
			zeros = work['zeros']
			success = False
			timestamp = work['timestamp']
			finish = int(work['finish'])
			while not success and nonce < finish :
				nonce += 1
				candidate = phash   + str(nonce) + timestamp
				sha = hashlib.sha256()
				sha.update(candidate.encode('utf-8'))
				candidate = sha.hexdigest()
				
				success = checkSha(candidate)
			sendToLider(nonce)



def liderWork():
	global actualWork,tries 
	actualWork = getWork()
	print (actualWork)
	while ld.imLider:
		try:
			txt , (addr, _) = sock.recvfrom(1024)
			txt = txt.decode('utf-8')
			if txt == 'ProcessRequest':
				sendWork(addr)
			elif txt[:16] == 'ProcessAnswerYes':
				txt = txt.split(";")
				if checkSha(txt[1]):
					tries = 0
					sendToServer(txt[1])
					#sendStopWork()
					actualWork = getWork()
					print(actualWork)
		except:
			pass
def askToLider():
	Recived = False
	sock.settimeout(6)
	paquet = "ProcessRequest".encode('utf-8')
	while not Recived and not ld.imLider:
		try:
			sockOut.sendto(paquet,(ld.lider,6001))
			resp = sock.recv(1000)
			resp = resp.decode('utf-8')
			resp = resp.split(';')
			resp = {'hash':resp[4],'zeros':resp[5],'start':resp[1],'finish':resp[2],'timestamp':resp[3]}
			print(resp)
			return resp
		except:
			print("sending")

def sendToServer(nonce):
	resp = requests.get('https://mineracao-facens.000webhostapp.com/submit.php?timestamp='+str(timestamp)+'&nonce='+str(nonce)+'&poolname=25170')
	print(resp.json())

def sendStopWork():
	paquet = "ProcessInterrupt".encode('utf-8')
	print(ld.workers)
	for key,value in ld.workers.items():
		type(value)
		#sock.sendto(paquet,(,6001))

def sendWork(addr):
	global tries
	start = startRange * tries
	
	finish = start + startRange
	
	paquet = "Process;"+str(start)+";"+str(finish)+";"+str(timestamp)+";"+str(actualWork['hash'])+";"+str(actualWork['zeros'])
	paquet = paquet.encode('utf-8')
	print("sending work to ", addr)
	sock.sendto(paquet,(addr,6001))
	tries = tries + 1
	

def initAll():
	pass

def getWork():
	global timestamp
	resp = requests.get(url)
	resp = resp.json()
	now = datetime.datetime.now()
	timestamp = now.microsecond
	return resp


def checkSha(nonce):
	print( "cheking: ", str(actualWork['hash']), " "  + str(nonce), ", " + str(timestamp) )
	candidate = str(actualWork['hash'])   + str(nonce) + str(timestamp)
	sha = hashlib.sha256()
	sha.update(candidate.encode('utf-8'))
	candidate = sha.hexdigest()
	number = candidate[:int(actualWork['zeros'])]
	Qn = ('0' * int(actualWork['zeros']))

	if number is Qn:
		return True

	return False




def sendToLider(nonce):
	paquet="ProcessAnswerYes;"+str(nonce)
	sock.sendto(paquet.encode('utf-8'),('192.168.0.33',6001))

def workerCheckSha(candidate):

	number = candidate[:int(zeros)]
	Qn = ('0' * int(zeros))

	if number is Qn:
		return True

	return False

if __name__ == '__main__':

	liderThread = LiderThread()
	liderThread.start()
	serviceLoop()